#!/bin/ash
container=`buildah from scratch`
chroot=`buildah unshare buildah mount $container`
buildah unshare apk add --no-scripts --no-commit-hooks --allow-untrusted --repositories-file `pwd`/repositories --initdb --root $chroot alpine-base podman buildah skopeo crun conmon
buildah unshare cp repositories $chroot/etc/apk/
buildah unshare cp libpod.conf $chroot/etc/containers/
buildah unshare cp storage.conf $chroot/etc/containers/
buildah commit $container alpinebuildah
